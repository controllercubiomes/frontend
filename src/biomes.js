// https://minecraft.gamepedia.com/Biome
// https://github.com/toolbox4minecraft/amidst/wiki/Biome-Color-Table

// 'name' is the official Minecraft name
// 'displayName' is the name used in Amidst and what is presented to the user
// 'label' is the name received from the api

export const biomes = [
  {
    id: 0,
    name: "Ocean",
    displayName: "Ocean",
    label: "ocean",
    hex: "#000070",
    enabled: true
  },
  {
    id: 1,
    name: "Plains",
    displayName: "Plains",
    label: "plains",
    hex: "#8db360",
    enabled: true
  },
  {
    id: 2,
    name: "Desert",
    displayName: "Desert",
    label: "desert",
    hex: "#fa9418",
    enabled: true
  },
  {
    id: 3,
    label: "mountains",
    name: "Mountains",
    displayName: "Extreme Hills",
    hex: "#606060",
    enabled: true
  },
  {
    id: 4,
    name: "Forest",
    displayName: "Forest",
    label: "forest",
    hex: "#056621",
    enabled: true
  },
  {
    id: 5,
    name: "Taiga",
    displayName: "Taiga",
    label: "taiga",
    hex: "#0b6659",
    enabled: true
  },
  {
    id: 6,
    name: "Swamp",
    displayName: "Swampland",
    label: "swamp",
    hex: "#07f9b2",
    enabled: true
  },
  {
    // TODO: Verify color
    // Amidst: true; WASM: false
    id: 7,
    name: "River",
    displayName: "River",
    label: "river",
    hex: "#0000ff",
    enabled: true
  },
  {
    // TODO: Verify color
    // Nether not implemented
    id: 8,
    displayName: "Hell",
    name: "Nether Wastes",
    label: "nether_wastes",
    hex: "#bf3b3b",
    enabled: false
  },
  {
    // TODO: Verify color
    // The End not implemented
    id: 9,
    name: "The End",
    displayName: "The End",
    label: "the_end",
    hex: "#8080ff",
    enabled: false
  },
  {
    id: 10,
    name: "Frozen Ocean",
    displayName: "Frozen Ocean",
    label: "frozen_ocean",
    hex: "#7070d6",
    enabled: true
  },
  {
    // TODO: Verify color
    // Amidst: true; WASM: false
    id: 11,
    name: "Frozen River",
    displayName: "Frozen River",
    label: "frozen_river",
    hex: "#a0a0ff",
    enabled: true
  },
  {
    id: 12,
    name: "Snowy Tundra",
    displayName: "Ice Plains",
    label: "snowy_tundra",
    hex: "#ffffff",
    enabled: true
  },
  {
    id: 13,
    name: "Snowy Mountains",
    displayName: "Ice Mountains",
    label: "snowy_mountains",
    hex: "#a0a0a0",
    enabled: true
  },
  {
    id: 14,
    name: "Mushroom Fields",
    displayName: "Mushroom Island",
    label: "mushroom_fields",
    hex: "#ff00ff",
    enabled: true
  },
  {
    id: 15,
    name: "Mushroom Field Shore",
    displayName: "Mushroom Island Shore",
    label: "mushroom_field_shore",
    hex: "#a000ff",
    enabled: true
  },
  {
    id: 16,
    name: "Beach",
    displayName: "Beach",
    label: "beach",
    hex: "#fade55",
    enabled: true
  },
  {
    id: 17,
    name: "Desert Hills",
    displayName: "Desert Hills",
    label: "desert_hills",
    hex: "#d25f12",
    enabled: true
  },
  {
    id: 18,
    name: "Wooded Hills",
    displayName: "Forest Hills",
    label: "wooded_hills",
    hex: "#22551c",
    enabled: true
  },
  {
    id: 19,
    name: "Taiga Hills",
    displayName: "Taiga Hills",
    label: "taiga_hills",
    hex: "#163933",
    enabled: true
  },
  {
    // TODO: Verify color
    // Amidst: false; WASM: false
    id: 20,
    name: "Mountain Edge",
    displayName: "Extreme Hills Edge",
    label: "mountain_edge",
    hex: "#72789a",
    enabled: true
  },
  {
    id: 21,
    displayName: "Jungle",
    name: "Jungle",
    label: "jungle",
    hex: "#537b09",
    enabled: true
  },
  {
    id: 22,
    name: "Jungle Hills",
    displayName: "Jungle Hills",
    label: "jungle_hills",
    hex: "#2c4205",
    enabled: true
  },
  {
    id: 23,
    name: "Jungle Edge",
    displayName: "Jungle Edge",
    label: "jungle_edge",
    hex: "#628b17",
    enabled: true
  },
  {
    id: 24,
    name: "Deep Ocean",
    displayName: "Deep Ocean",
    label: "deep_ocean",
    hex: "#000030",
    enabled: true
  },
  {
    id: 25,
    name: "Stone Shore",
    displayName: "Stone Beach",
    label: "stone_shore",
    hex: "#a2a284",
    enabled: true
  },
  {
    id: 26,
    name: "Snowy Beach",
    displayName: "Cold Beach",
    label: "snowy_beach",
    hex: "#faf0c0",
    enabled: true
  },
  {
    id: 27,
    name: "Birch Forest",
    displayName: "Birch Forest",
    label: "birch_forest",
    hex: "#307444",
    enabled: true
  },
  {
    id: 28,
    name: "Birch Forest Hills",
    displayName: "Birch Forest Hills",
    label: "birch_forest_hills",
    hex: "#1f5f32",
    enabled: true
  },
  {
    id: 29,
    name: "Dark Forest",
    displayName: "Roofed Forest",
    label: "dark_forest",
    hex: "#40511a",
    enabled: true
  },
  {
    id: 30,
    name: "Snowy Taiga",
    displayName: "Cold Taiga",
    label: "snowy_taiga",
    hex: "#31554a",
    enabled: true
  },
  {
    id: 31,
    name: "Snowy Taiga Hills",
    displayName: "Cold Taiga Hills",
    label: "snowy_taiga_hills",
    hex: "#243f36",
    enabled: true
  },
  {
    id: 32,
    name: "Giant Tree Taiga",
    displayName: "Mega Taiga",
    label: "giant_tree_taiga",
    hex: "#596651",
    enabled: true
  },
  {
    id: 33,
    name: "Giant Tree Taiga Hills",
    displayName: "Mega Taiga Hills",
    label: "giant_tree_taiga_hills",
    hex: "#454f3e",
    enabled: true
  },
  {
    id: 34,
    name: "Wooded Mountains",
    displayName: "Extreme Hills+",
    label: "wooded_mountains",
    hex: "#507050",
    enabled: true
  },
  {
    id: 35,
    name: "Savanna",
    displayName: "Savanna",
    label: "savanna",
    hex: "#bdb25f",
    enabled: true
  },
  {
    id: 36,
    name: "Savanna Plateau",
    displayName: "Savanna Plateau",
    label: "savanna_plateau",
    hex: "#a79d64",
    enabled: true
  },
  {
    id: 37,
    displayName: "Mesa",
    name: "Badlands",
    label: "badlands",
    hex: "#d94515",
    enabled: true
  },
  {
    id: 38,
    name: "Wooded Badlands Plateau",
    displayName: "Mesa Plateau F",
    label: "wooded_badlands_plateau",
    hex: "#b09765",
    enabled: true
  },
  {
    id: 39,
    name: "Badlands Plateau",
    displayName: "Mesa Plateau",
    label: "badlands_plateau",
    hex: "#ca8c65",
    enabled: true
  },
  {
    id: 40,
    name: "Small End Islands",
    displayName: "Small End Islands",
    label: "small_end_islands",
    hex: "#8080ff",
    enabled: false
  },
  {
    id: 41,
    name: "End Midlands",
    displayName: "End Midlands",
    label: "end_midlands",
    hex: "#8080ff",
    enabled: false
  },
  {
    id: 42,
    name: "End Highlands",
    displayName: "End Highlands",
    label: "end_highlands",
    hex: "#8080ff",
    enabled: false
  },
  {
    id: 43,
    name: "End Barrens",
    displayName: "End Barrens",
    label: "end_barrens",
    hex: "#8080ff",
    enabled: false
  },
  {
    id: 44,
    name: "Warm Ocean",
    displayName: "Warm Ocean",
    label: "warm_ocean",
    hex: "#0000ac",
    enabled: true
  },
  {
    id: 45,
    name: "Lukewarm Ocean",
    displayName: "Lukewarm Ocean",
    label: "lukewarm_ocean",
    hex: "#000090",
    enabled: true
  },
  {
    id: 46,
    name: "Cold Ocean",
    displayName: "Cold Ocean",
    label: "cold_ocean",
    hex: "#202070",
    enabled: true
  },
  {
    // TODO: Verify color
    // Amidst: false; WASM: false
    id: 47,
    displayName: "Warm Deep Ocean",
    name: "Deep Warm Ocean",
    label: "deep_warm_ocean",
    hex: "#000050",
    enabled: true
  },
  {
    id: 48,
    displayName: "Lukewarm Deep Ocean",
    name: "Deep Lukewarm Ocean",
    label: "deep_lukewarm_ocean",
    hex: "#000040",
    enabled: true
  },
  {
    id: 49,
    name: "Deep Cold Ocean",
    displayName: "Cold Deep Ocean",
    label: "deep_cold_ocean",
    hex: "#202038",
    enabled: true
  },
  {
    id: 50,
    name: "Deep Frozen Ocean",
    displayName: "Frozen Deep Ocean",
    label: "deep_frozen_ocean",
    hex: "#404090",
    enabled: true
  },
  {
    // TODO: Verify color
    // The End not implemented
    id: 127,
    name: "The Void",
    displayName: "The Void",
    label: "the_void",
    hex: "#000000",
    enabled: false
  },
  {
    id: 129,
    name: "Sunflower Plains",
    displayName: "Sunflower Plains",
    label: "sunflower_plains",
    hex: "#b5db88",
    enabled: true
  },
  {
    id: 130,
    name: "Desert Lakes",
    displayName: "Desert M",
    label: "desert_lakes",
    hex: "#ffbc40",
    enabled: true
  },
  {
    id: 131,
    name: "Gravelly Mountains",
    displayName: "Extreme Hills M",
    label: "gravelly_mountains",
    hex: "#888888",
    enabled: true
  },
  {
    id: 132,
    name: "Flower Forest",
    displayName: "Flower Forest",
    label: "flower_forest",
    hex: "#2d8e49",
    enabled: true
  },
  {
    id: 133,
    label: "taiga_mountains",
    name: "Taiga Mountains",
    displayName: "Taiga M",
    hex: "#338e81",
    enabled: true
  },
  {
    id: 134,
    name: "Swamp Hills",
    displayName: "Swampland M",
    label: "swamp_hills",
    hex: "#2fffda",
    enabled: true
  },
  {
    id: 140,
    name: "Ice Spikes",
    displayName: "Ice Plains Spikes",
    label: "ice_spikes",
    hex: "#b4dcdc",
    enabled: true
  },
  {
    id: 149,
    name: "Modified Jungle",
    displayName: "Jungle M",
    label: "modified_jungle",
    hex: "#7ba331",
    enabled: true
  },
  {
    id: 151,
    name: "Modified Jungle Edge",
    displayName: "Jungle Edge M",
    label: "modified_jungle_edge",
    hex: "#8ab33f",
    enabled: true
  },
  {
    id: 155,
    name: "Tall Birch Forest",
    displayName: "Birch Forest M",
    label: "tall_birch_forest",
    hex: "#589c6c",
    enabled: true
  },
  {
    id: 156,
    name: "Tall Birch Hills",
    displayName: "Birch Forest Hills M",
    label: "tall_birch_hills",
    hex: "#47875a",
    enabled: true
  },
  {
    id: 157,
    name: "Dark Forest Hills",
    displayName: "Roofed Forest M",
    label: "dark_forest_hills",
    hex: "#687942",
    enabled: true
  },
  {
    id: 158,
    name: "Snowy Taiga Mountains",
    displayName: "Cold Taiga M",
    label: "snowy_taiga_mountains",
    hex: "#597d72",
    enabled: true
  },
  {
    id: 160,
    name: "Giant Spruce Taiga",
    displayName: "Mega Spruce Taiga",
    label: "giant_spruce_taiga",
    hex: "#818e79",
    enabled: true
  },
  {
    id: 161,
    name: "Giant Spruce Taiga Hills",
    displayName: "Mega Spruce Taiga (Hills)",
    label: "giant_spruce_taiga_hills",
    hex: "#6d7766",
    enabled: true
  },
  {
    id: 162,
    name: "Gravelly Mountains+",
    displayName: "Extreme Hills+ M",
    label: "modified_gravelly_mountains",
    hex: "#789878",
    enabled: true
  },
  {
    id: 163,
    name: "Shattered Savanna",
    displayName: "Savanna M",
    label: "shattered_savanna",
    hex: "#e5da87",
    enabled: true
  },
  {
    id: 164,
    name: "Shattered Savanna Plateau",
    displayName: "Savanna Plateau M",
    label: "shattered_savanna_plateau",
    hex: "#cfc58c",
    enabled: true
  },
  {
    id: 165,
    name: "Eroded Badlands",
    displayName: "Mesa (Bryce)",
    label: "eroded_badlands",
    hex: "#ff6d3d",
    enabled: true
  },
  {
    id: 166,
    name: "Modified Wooded Badlands Plateau",
    displayName: "Mesa Plateau F M",
    label: "modified_wooded_badlands_plateau",
    hex: "#d8bf8d",
    enabled: true
  },
  {
    id: 167,
    name: "Modified Badlands Plateau",
    displayName: "Mesa Plateau M",
    label: "modified_badlands_plateau",
    hex: "#f2b48d",
    enabled: true
  },
  {
    id: 168,
    name: "Bamboo Jungle",
    displayName: "Bamboo Jungle",
    label: "bamboo_jungle",
    // hex: "#768e14" // Amidst's
    hex: "#9eb63c", // Null's
    enabled: true
  },
  {
    id: 169,
    name: "Bamboo Jungle Hills",
    displayName: "Bamboo Jungle Hills",
    label: "bamboo_jungle_hills",
    // hex: "#3b470a" // Amidst's
    hex: "#636f32", // Null's
    enabled: true
  },
  {
    // TODO: Verify color
    // Nether not implemented
    id: 170,
    name: "Soul Sand Valley",
    displayName: "Soul Sand Valley",
    label: "soul_sand_valley",
    hex: "#5e3830",
    enabled: false
  },
  {
    // TODO: Verify color
    // Nether not implemented
    id: 171,
    name: "Crimson Forest",
    displayName: "Crimson Forest",
    label: "crimson_forest",
    hex: "#dd0808",
    enabled: false
  },
  {
    // TODO: Verify color
    // Nether not implemented
    id: 172,
    name: "Warped Forest",
    displayName: "Warped Forest",
    label: "warped_forest",
    hex: "#49907b",
    enabled: false
  },
  {
    // TODO: Verify color
    // Nether not implemented
    id: 173,
    name: "Basalt Deltas",
    displayName: "Basalt Deltas",
    label: "basalt_deltas",
    hex: "#403636",
    enabled: false
  }
];

export function biomeLabels() {
  const labels = {};
  biomes
    .sort((a, b) => {
      if (a.displayName < b.displayName) return -1;
      if (a.displayName > b.displayName) return 1;
      return 0;
    })
    .forEach(biome => {
      if (!biome.enabled) return;
      labels[biome.label] = biome.displayName;
    });
  return labels;
}

export function biomeLabels2() {
  const labels = {};
  biomes.forEach(biome => {
    if (biome.enabled) {
      labels[biome.label] = biome.name;
    }
  });
  return labels;
}
