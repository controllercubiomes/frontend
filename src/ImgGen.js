/* global BigInt */

export default class ImgGen {

  queue;

  constructor() {
    this.queue = [this.init()];
  }

  async init() {
    const response = await fetch("/gen_image.wasm");
    const wasm = await response.arrayBuffer();
    const gen_image = await WebAssembly.compile(wasm);
    this.instance = await WebAssembly.instantiate(gen_image, {
      wasi_snapshot_preview1: {
        fd_write() { },
        fd_close() { },
        fd_seek() { },
        proc_exit() { },
      },
    });

    this.canvas = document.getElementById("c");
    this.ctx = this.canvas.getContext("2d");
    const pointer = this.instance.exports.get_buffer();
    const data = new Uint8ClampedArray(
      this.instance.exports.memory.buffer,
      pointer,
      512 * 512 * 4
    );
    this.img = new ImageData(data, 512, 512);

    // fall back to 52 bit number support in safari
    if (typeof BigInt !== "function") {
      window.BigInt = Number;
    }
  }

  gen(inputString) {
    const seed = BigInt(inputString);

    // extract low and high 32 bits because js ints are stupid
    const lo = Number(seed & BigInt(0xffffffff)) >>> 0;
    const hi = Math.floor(Number(seed / BigInt(4294967296))) >>> 0;

    this.instance.exports.gen_image(lo, hi);
    this.ctx.putImageData(this.img, 0, 0);

    return this.canvas.toDataURL();
  }

  addToQueue(seed) {

    const promise = new Promise((resolve) => {
      Promise.all(this.queue).then(() => {
        window.requestAnimationFrame(() => {
          const src = this.gen(seed);
          this.queue.shift();
          resolve(src);
        });
      });
    });

    this.queue.push(promise);

    return promise;
  }
}
