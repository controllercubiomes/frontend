const groupDbMap = {
  desert_group: "group_desert",
  forest_group: "group_forest",
  jungle_group: "group_jungle",
  mega_taiga_group: "group_megataiga",
  mesa_group: "group_mesa",
  mountain_group: "group_mountain",
  mushroom_group: "group_mushroom",
  ocean_group: "group_ocean",
  plains_group: "group_plains",
  roofed_group: "group_roofed_forest",
  savanna_group: "group_savanna",
  snowy_group: "group_snowy",
  swamp_group: "group_swamp",
  taiga_group: "group_tiga"
};

export default {
  namespaced: true,

  state: {
    filters: [],
    sortBy: null,
    isSortAscend: false
  },

  getters: {
    // Any change in this object will trigger a watcher to filter/sort the seeds
    filterOptions(state) {
      return {
        filters: state.filters,
        sort: state.sortBy,
        ascending: state.isSortAscend
      };
    },
    apiFilterBody(state) {
      // SUM(huts, monuments) < 4 && spawn_biome(savana, plains) || spawn_biome = ice_spikes || AVG(ovean, river, desert) < 10 && (ocean < 15 || warm_ocean > 10 && ocean < 20)

      // loop over each filter object and return a string
      const array = state.filters.map(filter => {

        if (filter.property === "spawn_biome") {
          // wrap spawn biomes together inside parentheses and delimit with a comma
          return `spawn_biome(${filter.value.join(",")})`;
        }

        if (filter.property === "all_groups") {
          const filters = [];
          for (const group in groupDbMap) {
            if (group !== 'mushroom_group') {
              filters.push(`${groupDbMap[group]}${filter.operator}${filter.value}`);
            }
          }
          return filters.join(' && ');
        }

        // we need to unify backend group names with frontend
        let field = filter.property;
        if (field.includes("_group")) {
          field = groupDbMap[field];
        }

        return `${field}${filter.operator}${filter.value}`;

      });

      // Join the filter strings together with an 'AND' operator
      const filterQueryString = array.join(" && ");

      // console.log(filterQueryString); //TODO: remove

      let sort = state.sortBy || "";
      if (sort.includes("_group")) {
        sort = groupDbMap[sort];
      }

      return {
        filter: filterQueryString.length ? filterQueryString : "",
        sort: [
          {
            field: sort,
            direction: state.isSortAscend ? "asc" : "desc"
          }
        ]
      };
    },
    apiFilterBodyAdvanced(state) {

      let sort = state.sortBy || "";
      if (sort.includes("_group")) {
        sort = groupDbMap[sort];
      }

      return {
        filter: state.filters[0] ? state.filters[0] : "",
        sort: [
          {
            field: sort,
            direction: state.isSortAscend ? "asc" : "desc"
          }
        ]
      };
    }
  },

  mutations: {
    setFilters(state, filters) {
      state.filters = filters;
    },
    addFilter(state, filter) {
      state.filters.push(filter);
    },
    removeFilter(state, index) {
      state.filters.splice(index, 1);
    },
    editFilter(state, filter) {
      for (let i = 0; i < state.filters.length; i++) {
        if (state.filters[i].unique === filter.unique) {
          state.filters.splice(i, 1, filter);
        }
      }
    },
    setSortBy(state, sortByString) {
      state.sortBy = sortByString;
    },
    setIsSortAscend(state, bool) {
      state.isSortAscend = bool;
    }
  },

  actions: {
    setFilters({ commit }, payload) {
      commit("setFilters", payload);
    },
    addFilter({ commit }, payload) {
      commit("addFilter", payload);
    },
    removeFilter({ commit }, payload) {
      commit("removeFilter", payload);
    },
    editFilter({ commit }, payload) {
      commit("editFilter", payload);
    },
    setSortBy({ commit }, payload) {
      commit("setSortBy", payload);
    },
    setIsSortAscend({ commit, state }, payload) {
      if (!state.sortBy) return;
      commit("setIsSortAscend", payload);
    }
  }
};
